package de.ovm.nxt;
import de.ovm.nxt.sequences.Abstand;
import de.ovm.nxt.sequences.Alle;
import de.ovm.nxt.sequences.Ausparken;
import de.ovm.nxt.sequences.Dance;
import de.ovm.nxt.sequences.Einparken;
import de.ovm.nxt.sequences.Klatschen;
import de.ovm.nxt.sequences.Musik;
import lejos.nxt.*;
import lejos.robotics.navigation.*;

/*
 * Autor: Lucas Vater
 */

public class NxtTest1{
	
	private Sensors sensors;
	private DifferentialPilot pilot;
	private SequencePlayer seqPlayer;
	
	public NxtTest1(){
		this.sensors = new Sensors();
		this.pilot = new DifferentialPilot(56, 115, Motor.C, Motor.B);
		pilot.setRotateSpeed(50);
		seqPlayer = new SequencePlayer();
		this.initSequences();
	}
	
	//F�gt alle Sequenzen zu Sequenz-Spieler hinzu
	private void initSequences(){
		seqPlayer.addSequence(new Alle(seqPlayer));
		seqPlayer.addSequence(new Einparken(pilot));
		seqPlayer.addSequence(new Ausparken(pilot));
		seqPlayer.addSequence(new Dance(sensors));
		seqPlayer.addSequence(new Klatschen(pilot, sensors));
		seqPlayer.addSequence(new Abstand(pilot, sensors));
		seqPlayer.addSequence(new Musik());
	}
	
	public void run(){
		seqPlayer.run(); //F�hrt den Sequenz-Spieler aus
	}
	
	
	public static void main(String[] args) throws Exception {
		NxtTest1 nx = new NxtTest1();
		Thread.sleep(200); //Pause, damit nicht gleich Knopfdruck registriert wird
		nx.run();
	}

}
