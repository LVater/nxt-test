package de.ovm.nxt;

import lejos.nxt.Button;

/*
 * Autor: Lucas Vater
 */

//Wird f�r das Men� benutzt, damit jeder Tastendruck nur einmal registriert wird

public class ButtonManager {
	
	private boolean right;
	private boolean left;
	private boolean enter;
	private boolean escape;
	
	
	public void update(){
		if(Button.RIGHT.isUp())right=false;
		if(Button.LEFT.isUp())left=false;
		if(Button.ENTER.isUp())enter=false;
		if(Button.ESCAPE.isUp())escape=false;
	}
	
	public boolean isRightPressed(){
		if(Button.RIGHT.isDown() && !right){
			right = true;
			return true;
		}
		return false;
	}
	
	public boolean isLeftPressed(){
		if(Button.LEFT.isDown() && !left){
			left = true;
			return true;
		}
		return false;
	}
	
	public boolean isEnterPressed(){
		if(Button.ENTER.isDown() && !enter){
			enter = true;
			return true;
		}
		return false;
	}
	
	public boolean isEscapePressed(){
		if(Button.ESCAPE.isDown() && !escape){
			escape = true;
			return true;
		}
		return false;
	}
}
