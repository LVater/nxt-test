package de.ovm.nxt;

import java.util.ArrayList;
import java.util.List;

import lejos.nxt.Button;
import de.ovm.nxt.sequences.ISequence;


/*
 * Autor: Lucas Vater
 */

public class SequencePlayer {
	public List<ISequence> sequences = new ArrayList<ISequence>(); //Liste von Sequenzen
	private Menu menu; //Men�
	
	public SequencePlayer(){
		this.menu = new Menu(this);
	}
	
	public void addSequence(ISequence sequence){ //F�gt Sequenz zu der Liste hinzu
		sequences.add(sequence);
	}
	
	public void run(){
		while(!Button.ESCAPE.isDown()){ //Solange Escape-Button nicht gedr�ckt ist
			menu.run(); //F�hre Menu aus
		}
		
	}
	
	public void playSequence(ISequence seq){ //Spiele Sequenz
		if(seq != null){
			seq.run();
		}
	}
}
