package de.ovm.nxt.sequences;

import de.ovm.nxt.Sensors;
import lejos.robotics.navigation.DifferentialPilot;

/*
 * Autor: Lucas Vater
 */

//Roboter f�hrt wenn geklatscht wird

public class Klatschen implements ISequence {
	
	private DifferentialPilot pilot;
	private Sensors sensors;
	
	public Klatschen(DifferentialPilot pilot, Sensors sensors){
		this.pilot = pilot;
		this.sensors = sensors;
	}
	
	@Override
	public void run() {
		long startTime = System.currentTimeMillis();
		long lastClap = 0;
		while(startTime+8000 > System.currentTimeMillis()){
			if(sensors.sound.readValue() > 36){
				if(lastClap+300 < System.currentTimeMillis()){
					pilot.travel(60);
					lastClap = System.currentTimeMillis();
				}
			}
			
		}
	}
	
	@Override
	public String getName() {
		return "Klatschen";
	}

}
