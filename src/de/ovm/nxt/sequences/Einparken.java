package de.ovm.nxt.sequences;

import lejos.robotics.navigation.DifferentialPilot;

/*
 * Autor: Lucas Vater
 */

//Parkt ein

public class Einparken implements ISequence {
	
	private DifferentialPilot pilot;
	
	public Einparken(DifferentialPilot pilot){
		this.pilot = pilot;
	}
	
	@Override
	public void run() {
		pilot.setRotateSpeed(50);
		pilot.arc(-180, 50);
		pilot.travel(-50);
		pilot.arc(40, -50);
	}
	
	@Override
	public String getName() {
		return "Einparken";
	}

}
