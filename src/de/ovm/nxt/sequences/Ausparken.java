package de.ovm.nxt.sequences;

import lejos.robotics.navigation.DifferentialPilot;

/*
 * Autor: Lucas Vater
 */

//Parkt Aus

public class Ausparken implements ISequence {
	
	private DifferentialPilot pilot;
	
	public Ausparken(DifferentialPilot pilot){
		this.pilot = pilot;
	}
	
	@Override
	public void run() {
		pilot.setRotateSpeed(50);
		pilot.arc(40, 50);
		pilot.travel(60);
		pilot.arc(-150, -50);
	}

	@Override
	public String getName() {
		return "Ausparken";
	}

}
