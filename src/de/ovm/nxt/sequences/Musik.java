package de.ovm.nxt.sequences;

import lejos.nxt.Sound;

/*
 * Autor: Lucas Vater
 */

//Spielt die Tetris Melodie

public class Musik implements ISequence {
	
	
	//Abwechselnd Frequenz und Dauer
	private int[] tetris = {330,1000, 247,500, 262,500,
			294,1000, 262,500, 247,500, 220,1000, 220,500, 262,500, 330,1000, 294,500, 262,500, 247,1000, 262,500, 
			294,1000, 330,1000, 262,1000, 220,1000, 220,2000};
	
	public Musik(){
	}
	
	@Override
	public void run() {
		
		int i=0;
		long lastNote = 0;
		int duration = 1000;
		while(i < tetris.length/2){
			while(lastNote > System.currentTimeMillis()){}
			int freq = tetris[i*2];
			duration = tetris[i*2+1]/2;
			
			Sound.playTone(freq, duration, 50);
			lastNote = System.currentTimeMillis()+duration;
			i++;
		}
	}

	@Override
	public String getName() {
		return "Musik";
	}

}
