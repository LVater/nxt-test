package de.ovm.nxt.sequences;

import de.ovm.nxt.Sensors;
import lejos.nxt.Motor;

/*
 * Autor: Lucas Vater
 */

//Roboter dreht sich und l�sst das Licht flackern

public class Dance implements ISequence {
	
	private Sensors sensors;
	
	public Dance(Sensors sensors){
		this.sensors = sensors;
	}
	
	@Override
	public void run() {
		long startTime = System.currentTimeMillis();
		Motor.B.setSpeed(800);
		Motor.C.setSpeed(800);
		Motor.B.forward();
		while(startTime+5000 > System.currentTimeMillis()){
			if(System.currentTimeMillis() % 100 == 0){
				sensors.light.setFloodlight(!sensors.light.isFloodlightOn());
			}
		}
		Motor.B.stop();
	}
	
	@Override
	public String getName() {
		return "Tanzen";
	}

}
