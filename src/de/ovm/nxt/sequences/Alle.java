package de.ovm.nxt.sequences;

import lejos.nxt.Button;
import de.ovm.nxt.SequencePlayer;

/*
 * Autor: Lucas Vater
 */

public class Alle implements ISequence {
	
	//F�hrt alle Sequenzen nach der Reihe aus(Au�er sich selbst)
	
	private SequencePlayer seqPlayer;
	
	public Alle(SequencePlayer seqPlayer){
		this.seqPlayer = seqPlayer;
	}
	
	@Override
	public void run() {
		int currentSeq = 1;
		long lastRun = 0;
		while(!Button.ESCAPE.isDown()){
			if(lastRun+2000 < System.currentTimeMillis()){
				ISequence seq = seqPlayer.sequences.get(currentSeq);
				if(seq != null){
					seqPlayer.playSequence(seq);
					currentSeq++;
					if(currentSeq >= seqPlayer.sequences.size())break;
					lastRun = System.currentTimeMillis();
				}
				
			}
		}
	}

	@Override
	public String getName() {
		return "Alle";
	}

}
