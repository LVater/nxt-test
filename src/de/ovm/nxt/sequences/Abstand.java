package de.ovm.nxt.sequences;

import de.ovm.nxt.Sensors;
import lejos.robotics.navigation.DifferentialPilot;

/*
 * Autor: Lucas Vater
 */

//H�lt Abstand von Objekten

public class Abstand implements ISequence {
	
	private DifferentialPilot pilot;
	private Sensors sensors;
	
	public Abstand(DifferentialPilot pilot, Sensors sensors){
		this.pilot = pilot;
		this.sensors = sensors;
	}
	
	@Override
	public void run() {
		
		int direction = 0;
		
		pilot.setTravelSpeed(100);
		float abs = 22;
		long startTime = System.currentTimeMillis();
		while(startTime+8000 > System.currentTimeMillis()){
			float curAbs = sensors.ultrasonic.getRange();
			
			float diff = abs-curAbs;
			
			if(Math.abs(diff) < 2){
				pilot.stop();
			}else{
				if(diff > 0){
					if(direction > -1 || !pilot.isMoving()){
						pilot.backward();
						direction = -1;
					}
				}else{
					if(direction < 1 || !pilot.isMoving()){
						pilot.forward();
						direction = 1;
					}
				}
			}
		}
		pilot.stop();
	}

	@Override
	public String getName() {
		return "Abstand";
	}

}
