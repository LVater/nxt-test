package de.ovm.nxt.sequences;

/*
 * Autor: Lucas Vater
 */

public interface ISequence { //Interface f�r Sequenzen
	public void run();
	public String getName();
}
