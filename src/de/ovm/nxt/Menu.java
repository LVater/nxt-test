package de.ovm.nxt;

import de.ovm.nxt.sequences.ISequence;
import lejos.nxt.LCD;

/*
 * Autor: Lucas Vater
 */

public class Menu {
	
	private SequencePlayer seqPlayer;
	private ButtonManager btnManager;
	private int currentSelection; //Aktuelle Auswahl
	
	public Menu(SequencePlayer seqPlayer){
		this.seqPlayer = seqPlayer;
		this.currentSelection = 0;
		this.btnManager = new ButtonManager();
	}
	
	public void run(){
		
		//Men� Zeichnen
		int i = 0;
		for(ISequence seq: seqPlayer.sequences){
			if(currentSelection == i){
				LCD.drawString("-> "+seq.getName(), 0, i); //Pfeil bei Auswahl zeichnen
			}else{
				LCD.drawString(seq.getName(), 0, i); //Sonst nur Sequenzname
			}
			
			i++;
		}
		LCD.refresh();
		
		//Pfeil bewegen
		btnManager.update();
		if(btnManager.isRightPressed()){
			currentSelection++;
			LCD.clear();
		}
		if(btnManager.isLeftPressed()){
			currentSelection--;
			LCD.clear();
		}
		if(currentSelection < 0)currentSelection=seqPlayer.sequences.size()-1;
		if(currentSelection >= seqPlayer.sequences.size())currentSelection=0;
		
		//Best�tigen
		if(btnManager.isEnterPressed()){
			ISequence seq = seqPlayer.sequences.get(currentSelection);
			if(seq != null){
				LCD.clear();
				LCD.drawString("Fuert aus:", 0, 0);
				LCD.drawString(seq.getName(), 0, 1);
				LCD.refresh();
				seqPlayer.playSequence(seq);
				LCD.clear();
			}
			
		}
		
	}
}
