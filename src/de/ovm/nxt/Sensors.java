package de.ovm.nxt;
import lejos.nxt.*;

/*
 * Autor: Lucas Vater
 */

public class Sensors {
	
	public SoundSensor sound; //Sound-Sensor
	public UltrasonicSensor ultrasonic; //Ultrasonic-Sensor
	public TouchSensor touch; //Touch-Sensor
	public LightSensor light; //Licht-Sensor
	 
	public Sensors(){
		this.sound = new SoundSensor(SensorPort.S2);
		this.ultrasonic = new UltrasonicSensor(SensorPort.S4);
		this.touch = new TouchSensor(SensorPort.S1);
		this.light = new LightSensor(SensorPort.S3);
		this.initSensors();
	}
	
	public void initSensors(){
		this.light.setFloodlight(false); //Schaltet das Licht aus
	}
}
